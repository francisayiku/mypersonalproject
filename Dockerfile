# Pull base image.
FROM alpine:3.5 

# Install Java.
RUN \
  apt-get update -y && \
  apt-get install -y install default-jdk -y && \
  apt-get install -y maven && \
  rm -rf /var/lib/apt/lists/* && \
  rm -rf /var/cache/default-jdk

COPY MyPersonalApp/src/main/webapp /usr/src/app/
#COPY templates/index.html /usr/src/app/templates/

EXPOSE 5000

# Define default command.
CMD ["javac", "/usr/src/app/webapp/index.jsp"]
