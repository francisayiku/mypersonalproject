<!DOCTYPE html>
<html lang="en">
<head>
<title>Page Title</title>
<meta charset="UTF-10">
<meta name="viewport" content="width=device-width, intial-scale=1">
<style>
* {
    box-sizing: border-box;
}
body {
    font-family: 'Courier New', Courier, monospace;
    margin: 0;
}
/*Style the header */
.header {
    padding: 90px;
    text-align: center;
    background: aquamarine;
    color: ghostwhite;
}
/* Increase the font size of the h1 element */
.header h1 {
    font-size: 45px;
}
/* Style the top navigation bar */
.navbar {
    overflow: hidden;
    background-color: rgb(6, 7, 7);
}
/* Style the navigation bar links */
.navbar a {
    float: left;
    display: block;
    color: white;
    text-align: center;
    padding: 15px 23px;
    text-decoration: transparent;
}
/* Right-aligned link */
.navbar a.right {
    float: right;
}
/* Change color on hover */
.navbar a:hover {
    background-color: burlywood;
    color: black;
}
/* Cloumn container */
.row {
    display: flex;
    flex-wrap: wrap;
}
/* Create two unequal columns that sits next to each other */
/* Sidebar/left column */
.side {
    flex: 40%;
    background-color: darkgray;
    padding: 15px;
}
/* Main column*/
.main {
    flex: 60%;
    background-color: azure;
    padding: 15px;
}
/* Responsive layout - when the screen is less than 700px wide, make two columns stack on top of each other instead of next to each other */
@media screen and (max-width: 700px) {
    .row {
        flex-direction: column;
    }
}
/* Responsive layout - when the screen is less than 400px wide, make the navigation links stack on top of each other instead of next to each other */
@media screen and (max-width: 400px) {
    .navbar a {
        float: none;
        width: 100%;
    }
} 
/* Add some image */
.img {
    background-color: none;
    width: 100%;
    padding: 20px;
}
/* Footer */
.footer {
    padding: 20px;
    text-align: center;
    background: darkgreen
}
</style>
</head>
<body>
    <div class="header">
        <h1>WELCOME TO MY PERSONAL SITE</h1>
        <p>This site is for DevOps and other IT training.</p>
    </div>

    <div class="navbar">
        <a href="https://www.linkedin.com/in/francis-ayiku-35942416/">Profile</a>
        <a href="#">Link</a>
        <a href="#">Link</a>
        <a href="#" class="right">Contact Us</a>
    </div>

    <div class="row">
        <div class="side">
            <h2>About Me:</h2>
            <h5>Photo of Me</h5>
            <div class="img" src="me.jpg" style="height: 200px">Me</div>
            <p>I am a DevOps/Site Reliability and Cloud Engineer. I have six years of total IT experience and 3 years practicing DevOps. I curently work at 7-Eleven, Inc</p>
        </div>
        <div class="main">
            <h2>INTRODUCTION</h2>
            <h3>What is DevOps?</h3>
            <p>DevOps is a set of software development practices that combine software development and information technology
            operations to shorten the systems development life cycle while delivering features, fixes, and updates frequently in
            close alignment with business objectives.</p>
            <div class="img" src="DevOps1.png" style="height: 200px">DevOps Diagram</div>
        </div>
    </div>
</body>
</html>